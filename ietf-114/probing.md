# Unilateral DNS Probing between Recursive and Authoritative Servers



## IETF 113 DNS Privacy (July 2022)

Joey Salazar, Daniel Kahn Gillmor, and Paul Hoffman

[draft-ietf-dprive-unilateral-probing](https://dkg.gitlab.io/dprive-unilateral-probing/)

---

# Recap about `unilateral-probing`

- Guidance to both Authoritative Servers and Recursive Resolvers
- No explicit coordination

---

# draft-ietf-dprive-unilateral-probing Changelog: from -00 to -01

- Moved discussion of non-opportunistic encryption to an appendix
- Clarify state transitions when sending over encrypted transport
- Introduced new field `E-last-response[X]` for comparison with persistence

---

# Progress?

- PowerDNS Recursor 4.7.0 implements probing for ADoT
    - (authoritative for `powerdns.com` also offers ADoT)
- other implementers?

---

# Current outstanding questions

- Clarify error handling
- Standards track?
- Visualizing state transitions?

---

# Critique, Suggest, Contribute!

- Implementers/deployers: share the lessons you've learned!
- Mailing list reviews and comments
- Gitlab issues and pull requests

[https://gitlab.com/dkg/dprive-unilateral-probing](https://gitlab.com/dkg/dprive-unilateral-probing)
